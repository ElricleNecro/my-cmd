if has_key(g:NERDDelimiterMap, &filetype)
	autocmd BufEnter,BufRead,FileType * :execute 'imap <leader>xg <Esc>_"ay$o' . g:NERDDelimiterMap[&filetype]['leftAlt'] . '<Esc>kJo<Esc>!!lua -l os -l math -e "print(<C-R>a)"<cr>kJi'
	autocmd BufEnter,BufRead,FileType * :execute 'nmap <leader>xg _"ay$o' . g:NERDDelimiterMap[&filetype]['leftAlt'] . '<Esc>kJo<Esc>!!lua -l os -l math -e "print(<C-R>a)"<cr>kJ'
endif
